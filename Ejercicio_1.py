#Ejercicio 1: Escribe un programa que lea repetidamente números hasta
#que el usuario introduzca “ﬁn”. Una vez se haya introducido “ﬁn”,
#muestra por pantalla el total, la cantidad de números y la media de
#esos números. Si el usuario introduce cualquier otra cosa que no sea un
#número, detecta su fallo usando try y except, muestra un mensaje de
#error y pasa al número siguiente.
#Introduzca un número: 4
#Introduzca un número: 5
#Introduzca un número: dato erróneo
#Entrada inválida
#Introduzca un número: 7
#Introduzca un número: fin
#16 3 5.33333333333

print(" EJERCICIO 1")
cont=0
acum=0
terminar="fin"

cant=int(input(" Ingrese cantidad de numeros: ")) #cantidad de numeros
try:
    while cont<cant:
        num=input(" introduce numero: ")  #ingreso cadena
        if num != terminar:               # comparo cadenas
                Num=int(num) # realizo la conversion de la cadena a entero
                acum = acum + Num
                cont = cont + 1
        else:
            if num == terminar:
                print(" termino la ejecucion...") #termino la ejecucion, poruqe las cadenas son las mismas "fin"
                break
    print(" Sumatoria: ",acum)
    print(" Cantidad de numeros ingresados: ",cont)
    promedio=acum/cont
    print(" Media: ",float(promedio)) #convierto de int a float
except:
    print(" dato erroneo...introduzca un numero...") #cuando el dato ingresado no es un numero